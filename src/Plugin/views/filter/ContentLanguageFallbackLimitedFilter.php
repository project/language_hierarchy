<?php

namespace Drupal\language_hierarchy\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\query\Sql;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filters to the most relevant translation for the current content language.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("language_hierarchy_content_language_fallback_limited")
 */
class ContentLanguageFallbackLimitedFilter extends FilterPluginBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new LanguageFilter instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct($configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Enable by default; the disabled state is only to allow the filter to be
    // optionally applied when exposing it.
    $options['value']['default'] = '1';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if ($this->value) {
      $this->ensureMyTable();

      $langcode = $this->languageManager
        ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
        ->getId();

      $this->value = array_values($this->languageManager->getFallbackCandidates([
        'langcode' => $langcode,
        'operation' => 'query',
      ]));

      // Ensure the current language is included in the fallback candidates, as
      // it doesn't necessarily have to be.
      if (!in_array($langcode, $this->value, TRUE)) {
        $this->value = array_merge([$langcode], $this->value);
      }

      /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $query = $this->query;
      $qualified_field = "$this->tableAlias.$this->realField";
      if (count($this->value) === 1) {
        $query->addWhere($this->options['group'], $qualified_field, $this->value[0], '=');
      }
      else {
        $query->addWhere($this->options['group'], $qualified_field, $this->value, 'IN');

        // Tag so that the join can be applied at query time.
        // @see language_hierarchy_query_language_hierarchy_limit_alter()
        $query->addTag('language_hierarchy_limit');

        // Metadata cannot be stored in views query plugins, so stuff the
        // necessary metadata into the view object.
        // @see language_hierarchy_query_language_hierarchy_limit_alter()
        $data = $this->viewsData->getAll();
        $this->view->build_info['language_hierarchy_limit'][$qualified_field] = [
          'base_table' => $this->table,
          'base_field' => $data[$this->table]['table']['base']['field'],
          'lang_codes' => $this->value,
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function valueForm(&$form, FormStateInterface $form_state) {
    $exposed = $form_state->get('exposed');

    $form['value'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show only the most specific translation for the content language selected for a page.'),
      '#default_value' => $this->value,
    ];

    // This filter only supports the Sql backend.
    $query_plugin = $this->displayHandler->getPlugin('query');
    if (!$query_plugin instanceof Sql) {
      $form['value']['#disabled'] = TRUE;
      $form['value']['#default_value'] = '';
      $form['value']['#description'] = $this->t('This option only supports the <em>Sql</em> query backend.');
    }

    // @see \Drupal\views\Plugin\views\filter\BooleanOperator::valueForm()
    if (!empty($this->options['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
      $user_input = $form_state->getUserInput();
      if ($exposed && !isset($user_input[$identifier])) {
        $user_input[$identifier] = $this->value;
        $form_state->setUserInput($user_input);
      }
    }
  }

}
