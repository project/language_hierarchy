<?php

namespace Drupal\language_hierarchy\Plugin\views\sort;

use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides sorting by language relevance based on language_hierarchy.
 *
 * @ViewsSort("language_hierarchy_sort")
 */
class LanguageHierarchySort extends SortPluginBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructs a new LanguageHierarchySort instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LanguageManagerInterface $language_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('language_manager'));
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['order'] = ['default' => 'DESC'];
    return $options;
  }


  /**
   * {@inheritdoc}
   */
  public function query() {
    $this->ensureMyTable();

    // Get the current language.
    $langcode = $this->languageManager
      ->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)
      ->getId();

    $fallback_langcodes = array_values($this->languageManager->getFallbackCandidates([
      'langcode' => $langcode,
      'operation' => 'views_query',
    ]));

    // Ensure the current language is included in the fallback candidates, as
    // it doesn't necessarily have to be.
    if (!in_array($langcode, $fallback_langcodes, TRUE)) {
      $fallback_langcodes = array_merge([$langcode], $fallback_langcodes);
    }

    /** @var \Drupal\views\Plugin\views\query\Sql $query */
    $query = $this->query;
    $qualified_field = "$this->tableAlias.$this->realField";

    // Use case statements to sort based on list of fallback languages. Weights
    // are inverted so that descending relevance means languages later in the
    // fallback chain get sorted last.
    $case_statements = '';
    $max_weight = 0 - count($fallback_langcodes);
    foreach ($fallback_langcodes as $weight => $langcode) {
      $weight = 0 - $weight;
      $case_statements .= "WHEN {$qualified_field} = '{$langcode}' THEN {$weight} ";
    }
    $formula = "(CASE {$case_statements} ELSE {$max_weight} END)";

    // Add the field.
    $alias = $query->addField(NULL, $formula, $this->tableAlias . '_language_hierarchy');
    $query->addOrderBy(NULL, NULL, $this->options['order'], $alias);
  }

}
