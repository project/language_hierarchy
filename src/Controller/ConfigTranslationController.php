<?php

namespace Drupal\language_hierarchy\Controller;

use Drupal\config_translation\ConfigMapperManagerInterface;
use Drupal\config_translation\Controller\ConfigTranslationController as CoreConfigTranslationController;
use Drupal\config_translation\Exception\ConfigMapperLanguageException;
use Drupal\Core\Access\AccessManagerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\language\Config\LanguageConfigFactoryOverrideInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;

/**
 * Overrides the configuration translation overview.
 */
class ConfigTranslationController extends CoreConfigTranslationController {

  /**
   * The language configuration override service.
   *
   * @var \Drupal\language\Config\LanguageConfigFactoryOverrideInterface
   */
  protected $configFactoryOverride;

  /**
   * Constructs this ConfigTranslationController override.
   *
   * @param \Drupal\config_translation\ConfigMapperManagerInterface $config_mapper_manager
   *   The configuration mapper manager.
   * @param \Drupal\Core\Access\AccessManagerInterface $access_manager
   *   The menu link access service.
   * @param \Symfony\Component\Routing\Matcher\RequestMatcherInterface $router
   *   The dynamic router service.
   * @param \Drupal\Core\PathProcessor\InboundPathProcessorInterface $path_processor
   *   The inbound path processor.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\language\Config\LanguageConfigFactoryOverrideInterface $config_override
   *   The language configuration override service.
   */
  public function __construct(ConfigMapperManagerInterface $config_mapper_manager, AccessManagerInterface $access_manager, RequestMatcherInterface $router, InboundPathProcessorInterface $path_processor, AccountInterface $account, LanguageManagerInterface $language_manager, RendererInterface $renderer, LanguageConfigFactoryOverrideInterface $config_override) {
    parent::__construct($config_mapper_manager, $access_manager, $router, $path_processor, $account, $language_manager, $renderer);
    $this->configFactoryOverride = $config_override;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.config_translation.mapper'),
      $container->get('access_manager'),
      $container->get('router'),
      $container->get('path_processor_manager'),
      $container->get('current_user'),
      $container->get('language_manager'),
      $container->get('renderer'),
      $container->get('language.config_factory_override')
    );
  }

  /**
   * Language translations overview page for a configuration name.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Page request object.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   * @param string $plugin_id
   *   The plugin ID of the mapper.
   *
   * @return array
   *   Page render array.
   */
  public function itemPage(Request $request, RouteMatchInterface $route_match, $plugin_id) {
    /** @var \Drupal\config_translation\ConfigMapperInterface $mapper */
    $mapper = $this->configMapperManager->createInstance($plugin_id);
    $mapper->populateFromRouteMatch($route_match);

    $page = parent::itemPage($request, $route_match, $plugin_id);

    try {
      $original_langcode = $mapper->getLangcode();
    }
    catch (ConfigMapperLanguageException $exception) {
      $original_langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED;
    }
    $fake_request = $request->duplicate();

    foreach (array_diff(Element::children($page['languages']), [$original_langcode]) as $langcode) {
      if (!empty($page['languages'][$langcode]['operations']['#links']['edit'])) {
        $fake_route_match = RouteMatch::createFromRequest($fake_request);
        $mapper->populateFromRouteMatch($fake_route_match);
        $mapper->setLangcode($langcode);

        // Check this translation really does exist directly in the language.
        $exists_directly = FALSE;
        foreach ($mapper->getConfigNames() as $config_name) {
          /** @var \Drupal\language\Config\LanguageConfigOverride $override */
          $override = $this->configFactoryOverride->getOverride($langcode, $config_name);
          if ($override->getLangcode() === $langcode) {
            $exists_directly = TRUE;
          }
        }

        if (!$exists_directly) {
          // Replace the edit & delete operations with an add one.
          unset(
            $page['languages'][$langcode]['operations']['#links']['edit'],
            $page['languages'][$langcode]['operations']['#links']['delete']
          );
          $page['languages'][$langcode]['operations']['#links']['add'] = [
            'title' => $this->t('Add'),
            'url' => Url::fromRoute($mapper->getAddRouteName(), $mapper->getAddRouteParameters()),
          ];
        }
      }
    }

    return $page;
  }

}
