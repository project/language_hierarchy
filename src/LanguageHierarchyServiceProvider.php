<?php

namespace Drupal\language_hierarchy;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Overrides the language overrides config factory service to use fallbacks.
 */
class LanguageHierarchyServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('language.config_factory_override');
    $definition->setClass('Drupal\language_hierarchy\Config\LanguageHierarchyConfigFactoryOverride');

    $modules = $container->getParameter('container.modules');

    // Decorate locale.storage only if locale module is enabled.
    if (isset($modules['locale'])) {
      $container->register('language_hierarchy.string_database_storage_decorator', 'Drupal\language_hierarchy\StringDatabaseStorageDecorator')
        ->setDecoratedService('locale.storage')
        ->addArgument(new Reference('language_hierarchy.string_database_storage_decorator.inner'))
        ->addArgument(new Reference('language_manager'))
        ->addArgument(new Reference('database'));
    }
    // Add RouteSubscriber only if Config Translation module is enabled.
    if (isset($modules['config_translation'])) {
      $container->register('language_hierarchy.route_subscriber', 'Drupal\language_hierarchy\Routing\RouteSubscriber')
        ->addArgument(new Reference(('plugin.manager.config_translation.mapper')))
        ->addTag('event_subscriber');
    }
  }

}
