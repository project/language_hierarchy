<?php

namespace Drupal\language_hierarchy\Config;

use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\language\Config\LanguageConfigOverride;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines language configuration overrides.
 */
class LanguageHierarchyConfigOverride extends LanguageConfigOverride {

  /**
   * The language code that this override is for.
   *
   * The config storage may be for a fallback language.
   *
   * @var string
   */
  protected $langcode;

  /**
   * The storage used to save this configuration object.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $target_storage;

  /**
   * Constructs a language override object that may be for a fallback language.
   *
   * @param string $name
   *   The name of the configuration object being overridden.
   * @param string $langcode
   *   The name of the language the override is intended for (i.e. the more
   *   specific language, whereas the config override may actually have come
   *   from the source storage for a fallback language).
   * @param \Drupal\Core\Config\StorageInterface $source_storage
   *   A storage controller object to use for reading the configuration
   *   override.
   * @param \Drupal\Core\Config\StorageInterface $target_storage
   *   A storage controller object to use for writing the configuration
   *   override.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config
   *   The typed configuration manager service.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct($name, $langcode, StorageInterface $source_storage, StorageInterface $target_storage, TypedConfigManagerInterface $typed_config, EventDispatcherInterface $event_dispatcher) {
    parent::__construct($name, $source_storage, $typed_config, $event_dispatcher);
    $this->target_storage = $target_storage;
    $this->langcode = $langcode;
  }

  /**
   * {@inheritdoc}
   */
  public function save($has_trusted_data = FALSE) {
    // Swap storage used by the save method.
    $source_storage = $this->storage;
    $this->storage = $this->target_storage;
    parent::save($has_trusted_data);

    // Restore source storage.
    $this->storage = $source_storage;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    // Swap storage used by the delete method.
    $source_storage = $this->storage;
    $this->storage = $this->target_storage;
    parent::delete();

    // Restore source storage.
    $this->storage = $source_storage;
    return $this;
  }

}
