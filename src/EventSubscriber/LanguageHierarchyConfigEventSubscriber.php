<?php

namespace Drupal\language_hierarchy\EventSubscriber;

use Drupal\Core\Config\ConfigImporterEvent;
use Drupal\Core\Config\ConfigEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class LanguageHierarchyConfigEventSubscriber.
 *
 * @package Drupal\language_hierarchy
 */
class LanguageHierarchyConfigEventSubscriber implements EventSubscriberInterface {

  /**
   * This method is called when the config.importer.import event is dispatched.
   *
   * @param \Drupal\Core\Config\ConfigImporterEvent $event
   *   The config import event.
   */
  public function onConfigImport(ConfigImporterEvent $event) {
    language_hierarchy_update_priorities();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[ConfigEvents::IMPORT] = ['onConfigImport'];
    return $events;
  }

}
