<?php

namespace Drupal\Tests\language_hierarchy\Kernel\Core;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\locale\Kernel\LocaleStringTest as CoreLocaleStringTest;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the locale string storage, string objects and data API.
 *
 * @group language_hierarchy
 */
class LocaleStringTest extends CoreLocaleStringTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_translation',
    'language',
    'language_hierarchy',
    'locale',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // We have to skip parent::setUp() here and go straight to the
    // KernelTestBase since parent::setUp() has no way of calling
    // $this->installSchema() prior to the language creation.
    KernelTestBase::setUp();
    $this->installSchema('language_hierarchy', ['language_hierarchy_priority']);

    // Add a default locale storage for all these tests.
    $this->storage = $this->container->get('locale.storage');
    // Create two languages: Spanish and German.
    foreach (['es', 'de'] as $langcode) {
      ConfigurableLanguage::createFromLangcode($langcode)->save();
    }
    $this->installSchema('locale', [
      'locales_location',
      'locales_source',
      'locales_target',
    ]);
  }

}
