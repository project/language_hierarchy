<?php

namespace Drupal\Tests\language_hierarchy\Kernel\Core;

use Drupal\Tests\locale\Kernel\LocaleConfigSubscriberTest as CoreLocaleConfigSubscriberTest;

/**
 * Tests that shipped configuration translations are updated correctly.
 *
 * @group language_hierarchy
 */
class LocaleConfigSubscriberTest extends CoreLocaleConfigSubscriberTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_translation',
    'language',
    'language_hierarchy',
    'locale',
    'locale_test',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUpLanguages(): void {
    $this->installSchema('language_hierarchy', ['language_hierarchy_priority']);
    parent::setUpLanguages();
  }

}
