<?php

namespace Drupal\Tests\language_hierarchy\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests the locale.storage decorator.
 *
 * @group language_hierarchy
 */
class StringDatabaseStorageDecoratorTest extends KernelTestBase {

  /**
   * @var \Drupal\locale\StringStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'config_translation',
    'language',
    'language_hierarchy',
    'locale',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('language_hierarchy', ['language_hierarchy_priority']);

    // Create our languages and their respective fallbacks.
    $languages = [
      'en' => NULL,
      'es' => 'en',
      'es-MX' => NULL,
      'de' => 'en',
      'de-DE' => 'de',
    ];
    foreach ($languages as $langcode => $fallback) {
      $language = ConfigurableLanguage::createFromLangcode($langcode);
      $language->setThirdPartySetting('language_hierarchy', 'fallback_langcode', $fallback);
      $language->save();
    }

    // Install the locale schema.
    $this->installSchema('locale', [
      'locales_location',
      'locales_source',
      'locales_target',
    ]);

    // Setup our storage service.
    $this->storage = $this->container->get('locale.storage');
  }

  /**
   * Tests that strings fallback to the correct regions.
   *
   * @covers Drupal\language_hierarchy\StringDatabaseStorageDecorator::dbStringSelect
   */
  public function testStringFallback() {
    $strings = [
      'en' => 'Read the article',
      'de' => 'Artikel lesen',
    ];

    // Create the english source string.
    $source = $this->storage->createString([
      'source' => $strings['en'],
      'context' => $this->randomMachineName(20),
    ])->save();

    // Create the english translation.
    $translation = $this->storage->createTranslation([
      'lid' => $source->lid,
      'language' => 'en',
      'translation' => $strings['en'],
    ])->save();

    // Create the german translation.
    $translation = $this->storage->createTranslation([
      'lid' => $source->lid,
      'language' => 'de',
      'translation' => $strings['de'],
    ])->save();

    // Confirm each defined language maps to the expected string.
    $expected_strings = [
      'en' => $strings['en'],
      'es' => $strings['en'],
      'es-MX' => '',
      'de' => $strings['de'],
      'de-DE' => $strings['de'],
    ];

    foreach ($expected_strings as $langcode => $expected_string) {
      $translation = $this->storage->findTranslation([
        'language' => $langcode,
        'lid' => $source->lid,
      ]);

      $this->assertEquals($expected_string, $translation->getString());
    }
  }

  /**
   * Tests that string fallback is loaded by priority.
   *
   * @covers Drupal\language_hierarchy\StringDatabaseStorageDecorator::dbStringSelect
   */
  public function testStringFallbackPriority() {
    $strings = [
      'en' => 'Read the article',
      'de' => 'Artikel lesen',
      'de-DE' => 'Artikel lesen (Germany)',
    ];

    // Create the english source string.
    $source = $this->storage->createString([
      'source' => $strings['en'],
      'context' => $this->randomMachineName(20),
    ])->save();

    // Create the en translation.
    $translation = $this->storage->createTranslation([
      'lid' => $source->lid,
      'language' => 'en',
      'translation' => $strings['en'],
    ])->save();

    // Create the de-DE translation.
    $translation = $this->storage->createTranslation([
      'lid' => $source->lid,
      'language' => 'de-DE',
      'translation' => $strings['de-DE'],
    ])->save();

    // Create the de translation.
    $translation = $this->storage->createTranslation([
      'lid' => $source->lid,
      'language' => 'de',
      'translation' => $strings['de'],
    ])->save();

    // Confirm each defined language maps to the expected string.
    $expected_strings = [
      'en' => $strings['en'],
      'de' => $strings['de'],
      'de-DE' => $strings['de-DE'],
    ];

    foreach ($expected_strings as $langcode => $expected_string) {
      $translation = $this->storage->findTranslation([
        'language' => $langcode,
        'lid' => $source->lid,
      ]);
      $this->assertEquals($expected_string, $translation->getString());
    }
  }

}
