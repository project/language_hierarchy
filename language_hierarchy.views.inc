<?php
/**
 * @file
 * Views hooks for the Language Hierarchy module.
 */

/**
 * Implements hook_views_data_alter().
 */
function language_hierarchy_views_data_alter(&$data) {
  // Add filters for language with fallback, for content translations.
  foreach (\Drupal::entityTypeManager()->getDefinitions() as $entity_type_id => $entity_type) {
    if ($entity_type->hasHandlerClass('views_data')) {
      if ($lang_key = $entity_type->getKey('langcode')) {
        $tables = [];
        if ($current_data_table = $entity_type->getDataTable()) {
          $tables[] = $current_data_table;
        }
        if ($revision_data_table = $entity_type->getRevisionDataTable()) {
          $tables[] = $revision_data_table;
        }
        foreach ($tables as $table) {
          if (!empty($data[$table][$lang_key]['filter']['id']) && $data[$table][$lang_key]['filter']['id'] === 'language') {
            $data[$table]['language_hierarchy_content_language_fallback_limited_' . $lang_key] = [
              'title' => t('Most relevant translation (using fallback)'),
              'help' => t('Shows only the translation that is most specific to the content language.'),
              'filter' => array(
                'id' => 'language_hierarchy_content_language_fallback_limited',
              ),
              'real field' => $lang_key,
              'entity field' => $lang_key,
            ];
          }

          // Add sort handler for language relevancy.
          $data[$table]['language_hierarchy_sort'] = [
            'title' => t('Content language relevance'),
            'help' => t('Sort content by relevance to the content language within the configured language hierarchy.'),
            'sort' => [
              'id' => 'language_hierarchy_sort',
            ],
            'real field' => $lang_key,
            'entity field' => $lang_key,
          ];
        }
      }
    }
  }
}
